-- Deploy stream:users to pg
-- requires: appschema

BEGIN;

CREATE TABLE streamer.list(
	streamerID	integer		PRIMARY KEY,
	streamerName 	TEXT		NOT NULL,
	streamerEmail	TEXT		NOT NULL,
	streamerPhone	integer		NOT NULL
);


COMMIT;
