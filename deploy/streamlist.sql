-- Deploy stream:streamlist to pg
-- requires: appschema

BEGIN;

CREATE TABLE stream.list(
	streamID	Integer	PRIMARY KEY,
	streamerID	Integer NOT NULL,
	streamName	TEXT	NOT NULL,
	timestamp	TIMESTAMPTZ NOT NULL
);

COMMIT;
