from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.routing import Route
from starlette.responses import HTMLResponse
from fastapi import FastAPI

async def homepage(request):
    return HTMLResponse('<h1>Hello streamer!</h1>')


app = Starlette(debug=True, routes=[
    Route('/', homepage),
])
